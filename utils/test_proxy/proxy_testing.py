#!/usr/bin/env python3

import requests
from requests.auth import HTTPProxyAuth
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import user_agents



def test_whatsmyip():

	url = 'http://canihazip.com/s'
	page = call_without_proxy(url)
	page_proxy = call_with_proxy(url)

	assert page.raw != page_proxy.raw
	print('Your ip is {page.text}'.format(page=page))
	print('Your proxy ip is {page_proxy.text}'.format(page_proxy=page_proxy))


def test_GS():
	url = 'https://scholar.google.com'
	page = call_without_proxy(url)
	try: print(page.status_code)
	except Exception as e: print(e)
	page_proxy = call_with_proxy(url)
	try: print(page_proxy.status_code)
	except Exception as e: print(e)

def call_without_proxy(url):
	session = requests.Session()
	return session.get(url, timeout=3)

def call_with_proxy(url):
	session = requests.Session()
	user = "rdahis2"
	password = "RI<21soTTo>32"
	proxy_ip = "144.217.121.116:222"
	# http://rdahis2:RI<21soTTo>32@144.217.121.116:222

	proxies = {"http"  : "http://" + user + ":" + password + "@" + proxy_ip,
			   "https" : "https://" + user + ":" + password + "@" + proxy_ip}
	session.proxies = proxies
	return session.get(url, timeout=10)


if __name__ == '__main__':
	test_whatsmyip()
	test_GS()
