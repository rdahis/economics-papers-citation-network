#!/usr/bin/env python
import sqlite3
import pandas
import warnings; warnings.filterwarnings('ignore', r'The spaces in these column names will not be changed. In pandas versions')
import sys
db = sqlite3.connect(sys.argv[1])
for csv in sys.argv[2:]:
	print('adding ' + csv)
	data = pandas.read_csv(csv)
	data.to_sql('publications', db, if_exists='append', index=False)
db.close()
print('done')
