#!/usr/bin/env python3
import re
import os
import gitpath
import datetime
from sub.publication_wrapper import PublicationWrapper
import lib.scholar as scholar
from sub.gscholar_publication import GScholarPublication

from sub.models import Publication, GScholarPub
import sub.models as models
from multiprocessing.dummy import Pool

BASEDIR = gitpath.root() + '/'
CSVPATH = BASEDIR + 'input/test.csv'

THREADS = 50



def main_db():
	db = models.DB(BASEDIR + 'tmp/main_db.sqlite')
	publications_without_gscholar_ids = None
	with db.new_session_scope() as session:
		publications_without_gscholar_ids =  [p.id for p in _get_publications_without_gscholar(session)]
	def process_publication(pub_id):
		with db.new_session_scope() as session:
			publication = session.query(Publication).get(pub_id)
			try: publication.gscholar = _fetch_gscholar_pub(publication)
			except Exception as e: print(e); return e
			publication.date_searched = models.datetime.datetime.now()
			print('saving {}'.format(publication.title))
			session.add(publication)
			session.commit()
			print('saved')
	pool = Pool(THREADS)
	pool.map(process_publication, publications_without_gscholar_ids)
	pool.close()


def _fetch_gscholar_pub(publication):
	publication_wrp = PublicationWrapper(publication)
	try:
		gscholar_pub = scholar_py_find_publication(publication_wrp.author, publication_wrp.title, publication_wrp.year)
		gscholar = models.GScholarPub(**{k:v[0] for k, v in gscholar_pub.scholar_py_article.attrs.items() if not k == 'url_related'})
		try:
			m = re.search('\?q=related:([^:]*):', gscholar_pub.scholar_py_article.attrs['url_related'][0])
			gscholar.id = m.groups(1)[0]
		except Exception: pass
		return gscholar
	except request_manager.NotFound: print('not found {}'.format(publication.title))

def _get_publications_without_gscholar(session):
	q = session.query(Publication).outerjoin(GScholarPub).filter(GScholarPub.id == None, Publication.date_searched == None).order_by(Publication.id)
	q = q.limit(5000) # pegar caras sem gscholar
	print("fetching {} papers from google".format(q.count()))
	return q


def scholar_py_find_publication(author, title, year):
	''' Runs scholar.py query. Returns first article if exists or None. '''
	author = '; '.join(author)
	phrase = title

	query = scholar.SearchScholarQuery()
	query.set_author(author)
	query.set_phrase(phrase)
	#query.set_timeframe(year - 1, year + 1)
	query.set_num_page_results(1)

	querier = scholar.ScholarQuerier()
	settings = scholar.ScholarSettings()

	querier.apply_settings(settings)
	querier.send_query(query)
	assert (querier.articles and len(querier.articles) >= 1)

	return GScholarPublication(querier.articles[0])


import sub.request_manager as request_manager

if __name__ == '__main__':
	try:
		start = datetime.datetime.now()
		request_manager.my_requests_get.count = 0
		request_manager.enable_cache(BASEDIR)
		main_db()
	finally:
		print("{} requests in {}".format(request_manager.my_requests_get.count, datetime.datetime.now() - start))
	# import ipdb; ipdb.set_trace()
