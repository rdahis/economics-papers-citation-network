import lib.scholar as scholar
import random, time, math, datetime, os
import http.cookiejar


# PARAMETERS
SLEEP_BETWEEN_REQUESTS = 1
DISCOUNTING = True
PROXYING = True
MAX_RETRIES = 15


def PROXY_IPs():
	user = "rdahis2"
	password = "RI<21soTTo>32"
	IPS = ['144.217.121.104:222' ,'144.217.121.105:222' ,'144.217.121.106:222' ,'144.217.121.108:222' ,'144.217.121.107:222' ,'144.217.121.116:222' ,'144.217.121.113:222' ,'144.217.121.110:222' ,'144.217.121.114:222' ,'144.217.121.112:222' ,'144.217.121.109:222' ,'144.217.121.111:222' ,'144.217.121.115:222' ,'144.217.121.122:222' ,'144.217.121.118:222' ,'144.217.121.124:222' ,'144.217.121.123:222' ,'144.217.121.120:222' ,'144.217.121.117:222' ,'144.217.121.119:222' ,'144.217.121.121:222' ,'144.217.121.126:222' ,'144.217.121.133:222' ,'144.217.121.132:222' ,'144.217.121.128:222' ,'144.217.121.129:222' ,'144.217.121.125:222' ,'144.217.121.127:222' ,'144.217.121.131:222' ,'144.217.121.130:222']
	# proxy_ip = "random.megaproxy.proxyrack.com:222"
	proxy_ip = random.choice(IPS)

	proxies = {"http"  : "http://" + user + ":" + password + "@" + proxy_ip,
			   "https" : "https://" + user + ":" + password + "@" + proxy_ip}
	return proxies


START = datetime.datetime.now()


COOKIE_JAR_FILE = '' # 'tmp/cookies.txt'
COOKIE_JAR = None
if os.path.exists(COOKIE_JAR_FILE):
	COOKIE_JAR = http.cookiejar.MozillaCookieJar(filename=COOKIE_JAR_FILE)
	COOKIE_JAR.load()


def my_requests_get(*args, **kwargs):
	import requests # import here so that only my_requests is on global scope
	if COOKIE_JAR:
		kwargs['cookies'] = COOKIE_JAR
	if PROXYING:
		kwargs['proxies'] = PROXY_IPs()
	kwargs['headers'] = {
			'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
	req = do_request_with_retries(*args, **kwargs)
	req.from_cache = getattr(req, 'from_cache', None)
	try: _validate_captcha(req)
	except Captcha:
		assert not req.from_cache
		requests_cache.get_cache().delete_url(req.url)
		with open('/tmp/captcha.html', 'w') as f: f.write(req.text)
		raise
	if not req.from_cache:
		my_requests_get.count += 1
		discount = datetime.datetime.now() - START
		sleep_time = int( SLEEP_BETWEEN_REQUESTS/(2**(discount.seconds/(3600*8))) ) # a cada 8 horas reduz pela metade o tempo
		rand = (random.random() - 0.5)*sleep_time
		print('not cached, sleeping {} + {}'.format(sleep_time, rand))
		time.sleep(sleep_time + rand)  # TODO: Dormir usando while(time_passed < delay) ao inves de sleep puro
	else: print('cached req retrieved')
	_validate_not_found(req)
	return req
my_requests_get.count = 0

def do_request_with_retries(*args, **kwargs):
	import requests
	errors = []
	for retry in range(1,MAX_RETRIES):
		try:
			return requests.get(*args, timeout=10, **kwargs)
		#except requests.exceptions.ReadTimeout as e:
		except Exception as e:
			print('error {}, retrying... ({})'.format(e, retry))
			errors.append(e)
	raise ConnectionError(errors)
			

def _validate_captcha(req):
	if not (req.status_code == 200 and 'results' in req.text):
		print('Probably Captcha')
		raise Captcha((req, req.url, get_text_from_html(req.text)))

def get_text_from_html(html):
	from bs4 import BeautifulSoup
	soup = BeautifulSoup(html, 'lxml')
	# kill all script and style elements
	for script in soup(["script", "style"]):
		script.extract()    # rip it out
	text = soup.get_text()
	lines = (line.strip() for line in text.splitlines()) # break into lines and remove leading and trailing space on each
	chunks = (phrase.strip() for line in lines for phrase in line.split("  ")) # break multi-headlines into a line each
	text = '\n'.join(chunk for chunk in chunks if chunk) # drop blank lines
	return text


def _validate_not_found(req):
	if 'did not match any' in req.text:
		raise NotFound(req)

def _validate(req):
	_validate_captcha(req)
	_validate_not_found(req)

class NotFound(Exception): pass
class Captcha(Exception): pass
class ConnectionError(Exception): pass

import requests_cache
def enable_cache(BASEDIR):
	print('enabling cache!')
	monkey_patch_scholar_py()
	requests_cache.install_cache(BASEDIR + 'tmp/demo_cache')
	requests_cache.uninstall_cache()

def monkey_patch_scholar_py():
	'''Objective: add cache functionality to scholar.py'''
	scholar.Request = my_requests_get
	class build_opener_fake:
		def __init__(self, *k): pass
		def open(self, req):
			return hdl_fake(req)
	class hdl_fake:
		def __init__(self, req):
			self.req = req
		def read(self):
			return self.req.text.encode('utf-8')
		info = lambda self: 'Mocked'
		getcode, geturl = info, info

	scholar.build_opener = build_opener_fake