# coding: utf-8
from sqlalchemy import Column, Integer, MetaData, Table, Text, ForeignKey, DateTime, CheckConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class Publication(Base):
	__tablename__ = 'publications'
	# surrogate = Column('id', Integer, primary_key=True, nullable=False)
	id = Column('Accession Number', Integer, primary_key=True, nullable=False, unique=False)
	abstract = Column('Abstract', Text)
	author = Column('Author', Text)
	author_affiliation = Column('Author Affiliation', Text)
	availability = Column('Availability', Text)
	availability_note = Column('Availability Note', Text)
	degree = Column('Degree', Text)
	descriptors = Column('Descriptors', Text)
	digital_object_identifier = Column('Digital Object Identifier', Text)
	editor = Column('Editor', Text)
	festschrift_honoree = Column('Festschrift Honoree', Text)
	geographic_descriptors = Column('Geographic Descriptors', Text)
	geographic_region = Column('Geographic Region', Text)
	isbn = Column('ISBN', Text)
	issn = Column('ISSN', Text)
	keywords = Column('Keywords', Text)
	language = Column('Language', Text)
	named_person = Column('Named person', Text)
	publication_date = Column('Publication Date', Text)
	publication_type = Column('Publication Type', Text)
	publisher_information = Column('Publisher Information', Text)
	reprint_date = Column('Reprint Date', Text)
	source = Column('Source', Text)
	title = Column('Title', Text)
	update_code = Column('Update Code', Integer)
	date_searched = Column(DateTime, default=None, nullable=True)

class GScholarPub(Base):
	__tablename__ = 'gscholar_pub'
	id            = Column('related_article_url', Text, primary_key=True, nullable=True)
	publication_id = Column(Integer, ForeignKey(Publication.id), nullable=False)
	url           = Column(Text, nullable=True, primary_key=True, unique=True)
	title         = Column(Text, nullable=False, primary_key=True)

	num_versions  = Column(Integer, nullable=False)
	url_citation  = Column(Text, nullable=True)
	num_citations = Column(Integer, nullable=True)
	cluster_id    = Column(Text, nullable=True)
	year          = Column(Text, nullable=True)
	excerpt       = Column(Text, nullable=True)
	url_versions  = Column(Text, nullable=True)
	url_pdf       = Column(Text, nullable=True)
	url_citations = Column(Text, nullable=True)
	date_inserted = Column(DateTime, default=datetime.datetime.now)

	publication = relationship(Publication, backref=backref('gscholar', uselist=False))

	__table_args__ = ( CheckConstraint('NOT(related_article_url IS NULL AND url IS NULL AND title IS NULL)', name='Primary_Key_Should_Not_Null'),)

from contextlib import contextmanager

class DB:
	def __init__(self, file):
		self.engine = self._create_engine(file)
		Base.metadata.bind = self.engine
		
	def _create_engine(self):
		from sqlalchemy import create_engine as eng
		self.engine = eng('sqlite:///' + file)

	def new_session(self):
		import sqlalchemy.orm
		return sqlalchemy.orm.Session(bind=self.engine)

	@contextmanager
	def new_session_scope(self):
		session = self.new_session()
		try:
			yield session
			session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
			
class DB_PG(DB):
	def _create_engine(self):
		from sqlalchemy import create_engine
		database   =  'main'
		user       =  'root'
		password   =  '8a96e0829de0461656c90bf1fda4fc28'
		host       =  'bd.bizbank.com.br'
		dialect='postgres'
		#URL from doc 'dialect+driver://user:password@host:port/database'
		url = 'postgres://%s:%s@%s:%s/%s' % (user, password, host, 5432, database)
		return create_engine( url )
		
	def __init__(self):
		self.engine = self._create_engine()
		Base.metadata.bind = self.engine

if __name__ == '__main__':
	from sys import argv
	from os import environ
	db = DB(argv[1])
	if environ.get('DROP'):
		Base.metadata.drop_all()
	Base.metadata.create_all()

