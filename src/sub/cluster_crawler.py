def crawl_publication_citations(publication):
	gscholar_pub = scholar_py_find_publication(publication.author, publication.title, publication.year)
	cluster_crawler = ClusterCrawler(gscholar_pub)
	return cluster_crawler.crawl()

def save_to_fs(publication_id, html_list):
	publication_dir = BASEDIR + 'output/{}/'.format(publication_id)
	if not os.path.exists(publication_dir):
		os.mkdir(publication_dir)
	for page_num, html in enumerate(html_list):
		with open(publication_dir + str(page_num+1) + '.html', 'w') as f:
			f.write(html.text)

class ClusterCrawler:
	def __init__(self, gscholar_pub):
		self.main_pub = gscholar_pub
		self.url = gscholar_pub.citations_url
		self.count = gscholar_pub.get_citations_count()
		self.ITEMS_PER_PAGE = 10

	def crawl(self):
		for page_idx in range(0, self._number_of_paginations()):
			yield self._crawl_page(page_idx)

	def _crawl_page(self, page_idx):
		paginated_url = self.url + '&start=' + str(page_idx * self.ITEMS_PER_PAGE)
		req = my_requests_get(paginated_url)
		try:
			_validate(req)
		except Exception as e:
			requests_cache.get_cache().delete_url(req.url)
			raise
		return req


	def _number_of_paginations(self):
		return math.ceil(int(self.count)/self.ITEMS_PER_PAGE)
