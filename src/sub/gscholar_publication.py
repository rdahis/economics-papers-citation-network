class GScholarPublication:
	def __init__(self, scholar_py_article):
		self.scholar_py_article = scholar_py_article

	@property
	def citations_url(self):
		return self.scholar_py_article.attrs['url_citations'][0]

	def get_citations_count(self):
		return self.scholar_py_article.attrs['num_citations'][0]