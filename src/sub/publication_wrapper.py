class PublicationWrapper:
    def __init__(self, row):
        try: author, title, pub_type, source, acc_num = row['Author'], row['Title'], row['Publication Type'], row['Source'], row['Accession Number'] #ta orriveu isso ai
        except TypeError: author, title, pub_type, source, acc_num = row.author, row.title, row.publication_type, row.source, row.id
        self.author = [x.strip() for x in author.split(';')]
        self.title  = title
        self.pub_type = pub_type
        self.source = source
        self._parse_source()
        # XXX checar se o assession number eh unico em toda a base
        self.id = acc_num

    def _parse_source(self):
        import re
        self.journal = self._get_first(self.source.split(','))
        # date not robust, should test for year being in string
        date = self._get_first(re.findall(', ([^,.]*),', self.source))
        try: self.year = int(self._get_first(re.findall('(19|20)[0-9][0-9]', date))) # TODO: escrever um jeito melhor de extrair esse ano do source
        except TypeError: self.year = None
        self.volume = self._get_first(re.findall('v. ([^,]*),', self.source))
        self.issue = self._get_first(re.findall('iss. ([^,]*),', self.source))
        self.pages = self._get_first(re.findall('pp. ([^,]*)', self.source))

    def _get_first(self, list1, default=None):
        for item in list1:
            return item
        return default
