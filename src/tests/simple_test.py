import main
from sub.models import Publication, GScholarPub
from stringcase import snakecase, lowercase

import sub.request_manager as request_manager

def test_trivial():
	assert 1 == 1

def test_johnes():
    assert 'baleia' != 'mestre'


def test_wrong_null():   # scholar.py => wrong/null & manual => correct 1st
    request_manager.my_requests_get.count = 0
    #request_manager.enable_cache(main.BASEDIR)

    offending_item = {
            "title" : """The Structure and Scope of Appeals Procedures for Public Employees""", "update_code" : """199006""", "date_searched" : None,
            "id" : """56503""", "abstract" : None, "author" : """Ullman, Joseph C.; Begin, James P.""", "author_affiliation" : None,
            "availability" : """http//digitalcommons.ilr.cornell.edu.turing.library.northwestern.edu/ilrreview/""",
            "availability_note" : None, "degree" : None,
            "descriptors" : """Employment in the Public Sector          (8226)Collective Bargaining in the Public Sector          (8322)""",
            "digital_object_identifier" : None,
            "editor" : None, "festschrift honoree" : None, "Geographic Descriptors" : """U.S.""", "Geographic Region" : """Northern America""",
            "ISBN" : None, "ISSN" : """00197939""", "Keywords" : None, "Language" : None, "Named person" : None,
            "Publication Date" : """April 1970""", "Publication Type" : """Journal Article""", "Publisher Information" : None,
            "Reprint Date" : None, "Source" : """Industrial and Labor Relations Review, April 1970, v. 23, iss. 3, pp. 323-34"""}
    
    publication = Publication(  # "Ullman, Joseph C.; Begin, James P."    "The Structure and Scope of Appeals Procedures for Public Employees"
        **{snakecase(lowercase(k)):v for k, v in offending_item.items()}
    )
    main._fetch_gscholar_pub(publication)
    
    